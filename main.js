let todos = [];

/*--- HANDLE ADD FORM ---*/
const addInput = document.querySelector("#addInput");
const addForm = document.querySelector("#addForm");

addForm.addEventListener("submit", (event) => {
  event.preventDefault();
  if (addInput.value) {
    addToDo(addInput.value);
    addInput.value = "";
  }
});

/*--- HANDLE RENDER ---*/
function renderTodo() {
  // create todo-list node
  const todoList = document.querySelector("#todoList");
  todoList.innerHTML = "";

  // loop through "todo" array to handle render todo-items
  todos.forEach((todo) => {
    // todo-items keys
    const { id, label, isDone, isEditing } = todo || {};

    // todoItemContent: wrap todo-item content
    const todoItemContent = document.createElement("li");
    todoItemContent.className = `todo-item ${isDone ? "done" : ""}`;
    todoItemContent.id = id;

    // todoItemLabel: render todo-item label
    const todoItemLabel = document.createElement("span");
    todoItemLabel.className = "todo-label";
    todoItemLabel.innerText = label;

    // todoItemAction: wrap todo-item actions
    const todoItemAction = document.createElement("div");
    todoItemAction.className = "todo-action";

    // deleteBtnNode: button handle delete action
    const deleteBtnNode = document.createElement("button");
    deleteBtnNode.className = "btn btn-delete";
    deleteBtnNode.innerText = "Delete";
    deleteBtnNode.addEventListener("click", (event) => {
      event.preventDefault();
      deleteTodoId(id);
    });

    // editBtnNode: button handle edit action
    const editBtnNode = document.createElement("button");
    editBtnNode.className = "btn btn-edit";
    editBtnNode.innerText = "Edit";
    editBtnNode.addEventListener("click", (event) => {
      event.preventDefault();
      toggleEditView(id);
    });

    // doneBtnNode: button handle done action
    const doneBtnNode = document.createElement("button");
    doneBtnNode.className = "btn btn-done";
    doneBtnNode.innerText = isDone ? "Undone" : "Done";
    doneBtnNode.addEventListener("click", (event) => {
      event.preventDefault();
      updateTodoStatus(id);
    });

    // editInputNode: input handle get user edited-label
    const editInputNode = document.createElement("input");
    editInputNode.className = "input inputEdit";
    editInputNode.value = label;

    // saveBtnNode: button handle save user edited-label
    const saveBtnNode = document.createElement("button");
    saveBtnNode.className = "btn btn-save";
    saveBtnNode.innerText = "Save";

    // editFormNode: form cover & handle submit edited-label
    const editFormNode = document.createElement("form");
    editFormNode.className = "form editForm";
    editFormNode.addEventListener("submit", (event) => {
      event.preventDefault();
      if (editInputNode.value) {
        updateLabel(id, editInputNode.value);
        toggleEditView(id);
        editInputNode.value = "";
      }
    });

    // if "isEditing" true, render edit view with editFormNode
    if (isEditing) {
      editFormNode.appendChild(editInputNode);
      editFormNode.appendChild(saveBtnNode);
      todoItemContent.appendChild(editFormNode);
    }
    // if "isEditing" false, render info view with labelNode & actionNode
    else {
      todoItemAction.appendChild(deleteBtnNode);
      !isDone && todoItemAction.appendChild(editBtnNode);
      todoItemAction.appendChild(doneBtnNode);
      todoItemContent.appendChild(todoItemLabel);
      todoItemContent.appendChild(todoItemAction);
    }

    // add this todoItemNode into todoListNode
    todoList.appendChild(todoItemContent);
  });
}

/*--- FUNCTIONS ---*/

function loadTodosFromLocalStorage() {
  const storedTodos = localStorage.getItem("todos");
  if (storedTodos) {
    todos = JSON.parse(storedTodos);
    renderTodo();
  }
}

// save todo-list to localStorage
function saveTodosToLocalStorage() {
  localStorage.setItem("todos", JSON.stringify(todos));
}

// call loadTodosFromLocalStorage when reload
window.addEventListener("load", loadTodosFromLocalStorage);

// function handle add todo
function addToDo(addedValue) {
  const newTodo = {
    id: Date.now(),
    label: addedValue,
    isDone: false,
  };
  todos.unshift(newTodo);
  saveTodosToLocalStorage();
  renderTodo();
}

// function handle delete todo by Id
function deleteTodoId(id) {
  todos = todos.filter((todo) => todo.id !== id);
  saveTodosToLocalStorage();
  renderTodo();
}

// function handle update todo-status
function updateTodoStatus(id) {
  todos = todos.map((todo) => (todo.id === id ? { ...todo, isDone: !todo.isDone } : todo));
  saveTodosToLocalStorage();
  renderTodo();
}

// function handle toggle todo item's edit view
function toggleEditView(id) {
  todos = todos.map((todo) => (todo.id === id ? { ...todo, isEditing: !todo.isEditing } : todo));
  saveTodosToLocalStorage();
  renderTodo();
}

// function handel update todo item's label
function updateLabel(id, editedLabel) {
  todos = todos.map((todo) => (todo.id === id ? { ...todo, label: editedLabel } : todo));
  saveTodosToLocalStorage();
  renderTodo();
}